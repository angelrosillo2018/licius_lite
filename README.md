# licius_lite
# npm install
# create a .env file at root project and add firebase credentials for auth obtained from firebase console when we create an app
# go to facebook for developers and add an app, then add login product so we can get an app id that is required in the facebook sdk script at our index.html file

# to take credit card payments
# sign up to the square(https://developer.squareup.com/docs/testing/sandbox),create an app and a sandbox account, make a note of application Id and access token of the application that we'll need in our square component

# to take paypal payments
# sign up to the paypal developer(https://developer.paypal.com) create an app and add the checkout product, take note of credentials that we'll add in our paypal component 

# to take mercadopago
# create a link to take payments and add it to project(https://www.mercadopago.com.mx/tools/share?button_id=57e6f6f6-da00-44ba-a90b-5d6ed36d32e7)

# dependencies that we are using
# "axios": "^0.19.0",
# "firebase": "^7.1.0", 
# "react-loading": "^2.0.3",
# "react-paypal-button-v2": "^2.6.1"


