const express = require('express');
const mercadopago = require ('mercadopago');
const bodyParser = require('body-parser');
const crypto = require('crypto');
const squareConnect = require('square-connect');
const dotenv = require('dotenv');
dotenv.config();
const app = express();
const port = 4000;
var cors = require('cors');
app.use(cors())
// Set the Access Token
// const accessToken = process.env.ACCESS_TOKEN

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(__dirname));

// Set Square Connect credentials and environment
// const defaultClient = squareConnect.ApiClient.instance;

// Configure OAuth2 access token for authorization: oauth2
// const oauth2 = defaultClient.authentications['oauth2'];
// oauth2.accessToken = accessToken;

// Set 'basePath' to switch between sandbox env and production env
// sandbox: https://connect.squareupsandbox.com
// production: https://connect.squareup.com
// defaultClient.basePath = 'https://connect.squareupsandbox.com';

// app.post('/process-payment', async (req, res) => {
//   const request_params = req.body;

  // length of idempotency_key should be less than 45
//   const idempotency_key = crypto.randomBytes(22).toString('hex');

  // Charge the customer's card
//   const payments_api = new squareConnect.PaymentsApi();
//   const request_body = {
//     source_id: request_params.nonce,
//     amount_money: {
//       amount: 100, 
//       currency: 'USD'
//     },
//     idempotency_key: idempotency_key
//   };

//   try {
//     const response = await payments_api.createPayment(request_body);
//     res.status(200).json({
//       'title': 'Payment Successful',
//       'result': response
//     });
//   } catch(error) {
//     res.status(500).json({
//       'title': 'Payment Failure',
//       'result': error.response.text
//     });
//   }
// });
mercadopago.configure({
    access_token: 'TEST-4564792176027841-031002-addeb92404c42e570b5e79818fb2051a-322103874'
  });
  
  // Crea un objeto de preferencia
  let preference = {
    items: [
      {
        title: 'Mi producto',
        unit_price: 100,
        quantity: 1,
      }
    ],
    "back_urls": {
        "success": "http://localhost:3000/succsess",
        "failure": "http://localhost:3000/failure",
        "pending": "http://localhost:3000/pending"
    },
    "auto_return": "approved",
  };
  
  mercadopago.preferences.create(preference)
  .then(function(response){
      console.log(response)
  // Este valor reemplazará el string "$$init_point$$" en tu HTML
    global.init_point = response.body.init_point;
  }).catch(function(error){
    console.log(error);
  });
app.listen(
  port,
  () => console.log(`listening on - http://localhost:${port}`)
);