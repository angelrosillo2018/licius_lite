import React, { useState, useEffect } from 'react';

// import CreditCardInput from 'react-credit-card-input';
import ReactLoading from "react-loading";
import { PayPalButton } from 'react-paypal-button-v2';
// import MercadoPago from 'mercadopago-pay'
// import Square from './Square';


import PaymentButtons from '../../components/PaymentButtons/PaymentButtons';
// import cardLabels from './cardLabels.json';
import './PaymentForm.css';

// var mercadopago = require('mercadopago');
const PaymentForm = props => {
  const [isLoad, setLoad] = useState(false);
  const Square =  ( {paymentForm} ) => {
    const config = {
          // Initialize the payment form elements const PaymentForm = props => {
          //TODO: Replace with your sandbox application ID
          applicationId: process.env.REACT_APP_APLLICATION_ID,
          inputClass: 'sq-input',
          autoBuild: false,
          // Customize the CSS for SqPaymentForm iframe elements
          inputStyles: [{
              fontSize: '16px',
              lineHeight: '24px',
              padding: '16px',
              placeholderColor: '#a0a0a0',
              backgroundColor: 'transparent',
          }],
          // Initialize the credit card placeholders
          cardNumber: {
              elementId: 'sq-card-number',
              placeholder: 'Card Number'
          },
          cvv: {
              elementId: 'sq-cvv',
              placeholder: 'CVV'
          },
          expirationDate: {
              elementId: 'sq-expiration-date',
              placeholder: 'MM/YY'
          },
          postalCode: {
              elementId: 'sq-postal-code',
              placeholder: 'Postal'
          },
          // SqPaymentForm callback functions
          callbacks: {
              /*
              * callback function: cardNonceResponseReceived
              * Triggered when: SqPaymentForm completes a card nonce request
              */
              cardNonceResponseReceived: function (errors, nonce, cardData) {
              if (errors) {
                  // Log errors from nonce generation to the browser developer console.
                  console.error('Encountered errors:');
                  errors.forEach(function (error) {
                      console.error('  ' + error.message);
                  });
                  alert('Encountered errors, check browser developer console for more details');
                  return;
              } else if (!errors){
                props.valuesHandler({
                  isPaid: true
                });
              }
                //alert(`The generated nonce is:\n${nonce}`);
                // fetch('http://localhost:4000/process-payment', {
                //     method: 'POST',
                //     headers: {
                //       'Accept': 'application/json',
                //       'Content-Type': 'application/json'
                //     },
                //     body: JSON.stringify({
                //       nonce: nonce
                //     })
                //   })
                //   .catch(err => {
                //     alert('Network error: ' + err);
                //   })
                //   .then(response => {
                //     if (!response.ok) {
                //       return response.text().then(errorInfo => Promise.reject(errorInfo));
                //     }
                //     props.valuesHandler({
                //       isPaid: true
                //     });
                //     return response.text();
                //   })
                //   .then(data => {
                //     console.log(JSON.stringify(data));
                //   })
                //   .catch(err => {
                //     console.error(err);
                //   });
              }
            }
      }

      paymentForm = new paymentForm(config);
      paymentForm.build();
      const requestCardNonce = () =>{
          paymentForm.requestCardNonce();
      }

      return (
          <div id="form-container">
              <div id="sq-card-number"></div>
              <div className="third" id="sq-expiration-date"></div>
              <div className="third" id="sq-cvv"></div>
              <div className="third" id="sq-postal-code"></div>
              <button id="sq-creditcard" className="button-credit-card" onClick={requestCardNonce}> Pay $ 1.00</button>
          </div>
        
      )
  }
  const squarePayment = isLoad ? (
    <Square paymentForm={ window.SqPaymentForm }/>
) : (
   null
)
  useEffect(() => {
    let sqPaymentScript = document.createElement("script");
    // sandbox: https://js.squareupsandbox.com/v2/paymentform
    // production: https://js.squareup.com/v2/paymentform
    sqPaymentScript.src = "https://js.squareupsandbox.com/v2/paymentform";
    sqPaymentScript.type = "text/javascript";
    sqPaymentScript.async = false;
    sqPaymentScript.onload = () => {
      setLoad(true);
    };
    document.getElementsByTagName("head")[0].appendChild(sqPaymentScript);
  });
  const [paymentMethod, setPaymentMethod] = useState(null);
  const [showLoading, setShowLoading] = useState(true);

  const handlePayment = (event, method) => {
    props.paymentMethodHandler(event, method);
    setPaymentMethod(method);
    props.valuesHandler({successAlert: null});
  }

  // const handleCardValue = (event, key) => {
  //   props.valuesHandler({
  //     [key]: event.target.value
  //   });
  // }
  // const pay = () => {
  //   let callback=props.handlePaymentMercado
  //   let configurations = mercadopago.configure({
  //     access_token: 'TEST-4564792176027841-031002-addeb92404c42e570b5e79818fb2051a-322103874'
  //   })
  //   return mercadopago.payment.create( configurations, callback);
  // }
  
  if (props.isPaid === true) {
    return (
      <div className="text-center">
        <button
          type="button"
          className="btn btn-danger btn-lg"
          onClick={props.reportCreator}
        >
          Crear reporte
          </button>
      </div>
    );
  } else if (paymentMethod === 'paypal') {
    return (
      <div className="PayPalContainer">
        {showLoading ? <ReactLoading type='balls' color="#c81e32" /> : null}
        <PayPalButton
          amount='1'
          currency='MXN'
          onButtonReady={() => setShowLoading(false)}
          options={{
            clientId: "AXL5_dtEa6F4gSY8GmWeXa-pVsrB6VGyNwm3eSz-OxjUIRa7RHHVYRmtcQuUAVGnv80hz2a_lzc960C0",
            currency: "MXN"
          }}
          onSuccess={(details, data) => {
            props.valuesHandler({
              isPaid: true,
              orderID: data.orderID,
              payerName: details.payer.name.given_name,
            });
            
          }}
        />
      </div>
    )
  } else if (paymentMethod === 'mercadopago') {
    return (
      // <form action="/procesar-pago" method="POST">
      //   <script
      //   src="https://www.mercadopago.com.ar/integrations/v1/web-payment-checkout.js"
      //   data-preference-id="322103874-9c5afa00-8612-45fc-9e78-bdd44dfe95c4">
      //   </script>
      // </form>
      <a mp-mode="dftl" href="https://www.mercadopago.com.mx/checkout/v1/redirect?pref_id=322103874-9c5afa00-8612-45fc-9e78-bdd44dfe95c4" name="MP-payButton" className='button-credit-card' target="_blank" rel="noopener noreferrer">Pagar</a>
    )
    
  } else if (paymentMethod === 'creditCard') {
    return (
      <div className="text-center">
        {squarePayment}
        {/* <CreditCardInput
          className='CreditCardInput'
          cardNumberInputProps={{
            value: props.cardValues.cardNumber,
            onChange: event => { handleCardValue(event, 'cardNumber') }
          }}
          cardExpiryInputProps={{
            value: props.cardValues.cardExpiry,
            onChange: event => { handleCardValue(event, 'cardExpiry') }
          }}
          cardCVCInputProps={{
            value: props.cardValues.cardCVC,
            onChange: event => { handleCardValue(event, 'cardCVC') }
          }}
          customTextLabels={cardLabels}
        /> */}
      </div>
    )
  } else if (paymentMethod === 'coupon') {
    return <p>Cupón</p>
  } else {
    return <PaymentButtons paymentHandler={handlePayment} />
  }
};

export default PaymentForm;